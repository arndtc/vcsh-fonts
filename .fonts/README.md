## This contains directories with my favorite fonts.

The fonts are stored in $HOME
- `~/.fonts/`

Run the follwoing to update font cache
- `fc-cache`

Run following to list fonts
- `fc-list`

Here are some links for relevant information on using fonts in X:
-----------------------------------------------------------------
- http://ftp.x.org/pub/X11R6.8.0/doc/fonts1.html
- http://ftp.x.org/pub/X11R6.8.0/doc/fonts2.html
- http://ftp.x.org/pub/X11R6.8.0/doc/xset.1.html - Only needed if you put fonts in a none standard directory.


Favorite Fonts:
---------------
1. https://fonts.google.com/specimen/Ubuntu+Mono
  - Found this one is very compact and you can get a lot of text on the screen
2. http://nodnod.net/2009/feb/12/adding-straight-single-and-double-quotes-inconsola/
  - http://www.levien.com/type/myfonts/inconsolata.html
  - https://fonts.google.com/specimen/Inconsolata
  - I like version with teh straight quotes
3. https://en.wikipedia.org/wiki/Fixedsys
  - http://www.moviecorner.de/en/font-fixedsys-ttf/fixedsys-download
4. https://fonts.google.com/specimen/Oxygen+Mono
5. https://fonts.google.com/specimen/Roboto+Mono
  - I like the slach through the zero, but is larger than Ubuntu Mono


Other Fonts to consider:
------------------------
- https://en.wikipedia.org/wiki/Bitstream_Vera
  - https://www.gnome.org/fonts/
- http://www.proggyfonts.net/
- http://www.donationcoder.com/Software/Jibz/Dina/index.html
- https://github.com/chrismwendt/bront
  - Ubuntu Mono with slashed zero and few other minor tweaks
- http://sourcefoundry.org/hack/
  - Completely open source font.  Looks pretty promising


Websites for Comparing Fonts:
-----------------------------
- https://sourcefoundry.org/hack/playground.html
- https://fonts.google.com/
- https://fonts.google.com/download/next-steps
