##This contains directories with my favorite fonts.

The fonts are stored in ~/.fonts/

Here are some links for relevant information on using fonts in X:
-----------------------------------------------------------------
http://ftp.x.org/pub/X11R6.8.0/doc/fonts1.html
http://ftp.x.org/pub/X11R6.8.0/doc/fonts2.html
http://ftp.x.org/pub/X11R6.8.0/doc/xset.1.html - Only needed if you put fonts in a none standard directory.


Favorite Fonts:
---------------
http://nodnod.net/2009/feb/12/adding-straight-single-and-double-quotes-inconsola/
 - http://www.levien.com/type/myfonts/inconsolata.html
https://en.wikipedia.org/wiki/Fixedsys
 - http://www.moviecorner.de/en/font-fixedsys-ttf/fixedsys-download
https://www.google.com/fonts/specimen/Oxygen+Mono


Other Fonts:
------------
https://en.wikipedia.org/wiki/Bitstream_Vera
 - https://www.gnome.org/fonts/
http://www.proggyfonts.net/
http://www.donationcoder.com/Software/Jibz/Dina/index.html
http://sourcefoundry.org/hack/


Websites for Comparing Fonts:
-----------------------------
https://sourcefoundry.org/hack/playground.html
https://fonts.google.com/
